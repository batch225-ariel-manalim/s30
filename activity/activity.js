// 1.
db.fruits.aggregate([
	{$match: { onSale: true } // 1st phase
	},
	{ $count: "fruitsOnSale"}
	])


// 2.
db.fruits.aggregate([
	{$match: { stock : { $gte : 20 } }
},
{ $count: "enoughStock"}	
])

// 3.
db.fruits.aggregate ([
{
		$match: { onSale: true } // 1st phase
	},
	{
		$group: {
			_id: "$supplier_id",
			avg_rice: { $avg: "$price" }
		}
	}
	])


// 4.
db.fruits.aggregate ([
{
		$match: { onSale: true } // 1st phase
	},
	{
		$group: {
			_id: "$supplier_id",
			avg_rice: { $max: "$price" }
		}
	}
	])


// 5.
db.fruits.aggregate ([
{
		$match: { onSale: true } // 1st phase
	},
	{
		$group: {
			_id: "$supplier_id",
			avg_rice: { $min: "$price" }
		}
	}
	])